<?php
function prettifyJSON($json)
{
    $array = json_decode($json, true, 512, JSON_UNESCAPED_UNICODE);
    $json = json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    return $json;
}

function tored($string) {
	return "\033[31m$string\033[0m";
}

function toviolet($string) {
	return "\033[35m$string\033[0m";
}

function togreen($string) {
	return "\033[32m$string\033[0m";
}

function tocyan($string) {
	return "\033[96m$string\033[0m";
}

function tobold($string) {
	return "\033[1m$string\033[22m";
}

readline_completion_function(function($Input, $Index)
{
	global $Commands;

 	$Matches = array();
 	foreach( array_keys($Commands) as $Command )
		if( stripos($Command, $Input ) === 0)
			$Matches[] = $Command;
				$test[$Command] = stripos( $Command, $Input );

	// Prevent Segfault
	if( $Matches == false )
		$Matches[] = '';

	return $Matches;
});

function input(
	string $prompt = '',
	array $suggestions = [],
	bool $password = false
): string|int|array|bool
{
	if($password)
	{
		$GLOBALS['Commands'] = [];
		echo $prompt;
		system('stty -echo'); // Disable echo

		// This probably fails if password contains space as first or last char
		$input = trim(fgets(STDIN));

		system('stty echo'); // enable echo
		return $input;
	}

	$GLOBALS['Commands'] = $suggestions;
	$input = readline($prompt);
	return trim($input);
}
