<?php
class Huuto
{
	private $ch;
	private $headers;
	public $user;
	private $terminal;

	public function __construct()
	{
		$this->terminal = new Terminal();
		$this->ch = curl_init();
		$this->user = (object) [];

		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($this->ch, CURLOPT_HEADER, true);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false); // !!
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false); // !!
		curl_setopt($this->ch, CURLOPT_USERAGENT, HUUTOBOT_USERAGENT);

		$this->headers = [
			'accept: application/json',
			'Accept-Language: en-US,en;q=0.5',
			'Connection: keep-alive',
			'Host: api.huuto.net'
		];

		if( !is_dir(HOME . "/.huutonet/"))
		{
			echo "Initializing...\n";
			// First time starting this
			mkdir(HOME . "/.huutonet/");
			file_put_contents(HOME . "/.huutonet/accounts.json", json_encode([]));
            echo "Creating category file...\n";
			$categoryTree = prettifyJSON(json_encode($this->generateCategoryList()));
			file_put_contents(HOME . "/.huutonet/categories.json", $categoryTree);
			echo "Setup completed.";
			sleep(1);
		}
	}

	private function generateCategoryList( $id = 0, $int = 0 )
	{
		$url = "categories/$id/subcategories";
		$categories = json_decode($this->getRequest($url))->categories;
		$categoryTree = [];
		foreach( $categories as $category )
		{
			$id = $category->id;
			$categoryTree[$id] = [
				"title" => $category->title,
			];

            echo str_repeat("\t", $int) . $category->id.": " . $category->title . "\n";

			if( $category->links->subcategories )
			{
                $int++;
				$categoryTree[$id]["subcategories"] = $this->generateCategoryList( $id, $int );
                $int--;
			}
		}
		return $categoryTree;
	}

	private function denyWithoutLogin(): bool
	{
		if(!$user->username)
		{
			echo tored("This action is restricted.");
			echo "login to continue";
			return false;
		}
		return true;
	}

    public function browseCategories()
    {
        $GLOBALS['Commands'] = [];
        $categories = file_get_contents(BOTFILES."categories.json");
        $categories = json_decode($categories);
        $depth = [];
        while(true)
        {
            $stuff = $categories;
            foreach($depth as $itm)
                $stuff = $stuff->$itm->subcategories;

            foreach($stuff as $id => $category)
            {
                $GLOBALS['Commands'][$category->title] = $id;
                echo "\t" . $category->title."\n";
            }

            $input = trim(readline(">"));

            switch($input) {
            case "help":
                echo "back, ready";
                break;

			case "back":
				// No error handling, it would be just annoying notice
				if(count($depth) > 1)
					array_pop($depth);
				break;

			case "ready":
				return end($depth);
				break;

			default:
				$categoryID = $GLOBALS['Commands'][$input];

				if(isset($stuff->$categoryID))
					$depth[] = $categoryID;
				else
					echo tored("Category not found.") . "\n";
				break;
			}
		}
	}

	public function login($username, $password): bool
	{
		$this->user = (object) [];

		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($this->ch, CURLOPT_URL, HUUTOBOT_BASEURL . "authentication");
		$post = [
			"username" => $username,
			"password" => $password
		];
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($this->ch);
		if ($response === false) {
			echo tored("Conneceting to authentication endpoint failed.");
			echo curl_error($this->ch) . "\n";
			return false;
		}

		$code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
		if( !in_array($code, [200, 201]) ) {
			echo tored("Authentication failed.");
			echo "Response code: $code\n";
			return false;
		}

		echo togreen("Authentication completed") . "\n";
		$this->user->nick = $username;

		list($respHeaders, $body) = explode("\r\n\r\n", $response, 2);

		$json = json_decode($body);
		$this->user->authid = $json->authentication->token->id;
		$userpagelink = strval($json->links->user);

		$userid = explode("/", $userpagelink);
		$userid = end($userid);
		$this->user->id = $userid;
		$this->terminal->username = $username;

		$this->headers[] = "X-HuutoApiToken: " . $this->user->authid;

		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, false);
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "GET");
		return true;
	}

	private function getRequest($prefixurl)
	{
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($this->ch, CURLOPT_URL, HUUTOBOT_BASEURL . $prefixurl);
		$response = curl_exec($this->ch);
		list($headers, $body) = explode("\r\n\r\n", $response, 2);
		if ($response === false) {
			echo tored("Conneceting to authentication endpoint failed.\n");
			echo curl_error($this->ch) . "\n";
			return false;
		}
		return $body;
	}

    public function ownProfile()
    {
		if (!$this->denyWithoutLogin())
			return false;
		
		$this->viewProfile($this->user->id);
		return;
    }

    public function viewProfile( $id )
    {
		$this->terminal->clearScreen();
        $json = $this->getRequest("users/$id");
		$json = json_decode($json);

		echo "\n\n\n";

		echo "Commands:\n";
		echo "\tselling\n";
		echo "\tfeedback\n";

		echo "\n\n\n";

		foreach($json->address as $line)
			echo "* $line\n";

		echo "\n\n\n";

		echo $json->description;

		echo "\n\n\n";

		while(true)
		{
			$this->terminal->page = $json->username . "'s profile";
			$this->terminal->loadBasicScreen();
			$input = readline(">");
			switch($input)
			{
			case "help":
				echo "selling, feedback, exit";
				break;

			case "exit":
				return;
				break;

			case "selling":
				$this->browseSellersProducts($this->user->id);
				break;

			case "feedback":
				$this->readFeedback($this->user->id);
				break;

			default:
				break;
			}

		}
		return false; // ???
	}

	public function searchProducts(
		$addtime = null, // past-day, past-2days, past-5days, past-week
		$area = null, // "Helsinki", "Uusimaa", "00100"
		$category = 0,
		$condition = null, // none, new, like-new, good, acceptable, weak
		$closingtime = null, // next-day, next-2days, next-5days, next-week
		$feedback_limit = 0,
		$limit = 5,  // !!!!! At this time this parameter is not honored by huutonet api 1.1 !!!!!
		$page = 1,
		$price_max = null,
		$price_min = null,
		$seller_type = null, // company, user
		$seller_nro = null,
		$sellstyle = "all", // all, auction, buy-now
		$sort = "newest", // hits, newest, closing, lowprice, highprice, bidders, title
		$status = "open", // open, closed
		$words = ""
	)
	{

		$parameters = [
			"addtime" => $addtime,
			"area" => $area,
			"category" => $category,
			"condition" => $condition,
			"closingitme" => $closingtime,
			"feedback_limit" => $feedback_limit,
			"limit" => $limit,  // !!!!! At this time this parameter is not honored by huutonet api 1.1 !!!!!
			"page" => $page,
			"price_max" => $price_max,
			"price_min" => $price_min,
			"seller_type" => $seller_type,
			"seller_nro" => $seller_nro,
			"sellstyle" => $sellstyle,
			"sort" => $sort,
			"status" => $status,
			"words" => $words
		];

		$json = $this->getRequest("items?".http_build_query($parameters));
		return $json;
	}

	public function createSearchParameters()
	{
		// TODO: add type check
		$options = [
			"addtime" => null,
			"area" => null,
			"category" => null,
			"condition" => null,
			"closingitme" => null,
			"feedback_limit" => null,
			"limit" => null,
			"page" => null,
			"price_max" => null,
			"price_min" => null,
			"seller_type" => null,
			"seller_nro" => null,
			"sellstyle" => null,
			"sort" => null,
			"status" => null,
			"words" => null,
		];

		$suggestions = array_combine(array_keys($options), array_keys($options));
		$return = false;
		$this->terminal->page = "Search generator";
		while(!$return)
		{
			$this->terminal->loadBasicScreen();
			$input = input(">", $suggestions);

			switch($input) {
			case "help":
				echo "ready, status\n";
				break;

			case "ready":
				$return = true;
				break;

			case "category":
				$category = $this->browseCategories();
				$options["category"] = $category;
				break;

			case "read":
				foreach($options as $key => $option)
					echo "\t$key => $option\n";

				break;
			default:
				if( in_array($input, array_keys($options)) )
				{
					$value = readline("New value: ");
					$options[$input] = $value;
				}
				else
				{
					echo tored("Invalid option.") . "\n";
				}
				break;
			}
		}

		return array_filter($options);
	}

	public function browseSellersProducts( $id )
	{

	}

}
