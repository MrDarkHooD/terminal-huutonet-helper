<?php
Class Terminal
{
	public $username = null;
	public $page = null;
	private $minlines = 40;
	private $mincols = 60;

	public function __construct()
	{

	}

	public function clearScreen(): void
	{
		echo chr(27).chr(91).'H'.chr(27).chr(91).'J';
		return;
	}

	function readline_pw($prompt = "Enter Password: ") {
		echo $prompt;
		system('stty -echo');
		$password = trim(fgets(STDIN));
		system('stty echo');
		return $password;
	}

	public function loadBasicScreen()
	{
		// This function should be renamed
		// maybe "updateStaticScreenElements"

		$lines = strval(exec('tput lines'));

        echo "\033[1;1H";
        echo $this->page;

		if($this->username)
		{
			$cols = strval(exec('tput cols')-strlen("Logged in as ".$this->username)+1);
			echo "\033[1;{$cols}H";
			echo "Logged in as " . togreen($this->username);
		}
		else{
			$cols = strval(exec('tput cols')-strlen("Not logged in")+1);
			echo "\033[1;{$cols}H";
			echo "Not logged in";
		}

		if(
			exec('tput cols') < $this->mincols ||
			exec('tput lines') < $this->minlines
		)
		{
			echo "\033[2;1H";
			echo tored("You should make terminal bigger!");
		}

		echo "\033[{$lines};0H";

		return;
	}

}
