#!/bin/php
<?php
include("settings.php");
include("functions.php");
include("src/Terminal.class.php");
include("src/Huuto.class.php");
error_reporting(E_ALL);
define("HOME", $_SERVER['HOME']);
define("BOTFILES", $_SERVER['HOME']."/.huutonet/");

$terminal = new Terminal();
$terminal->clearScreen();
$huuto = new Huuto();

$commands = [
	"login" => "Login to huutonet",
	"register" => "Register to huutonet",
	"logout" => "Logout from huutonet",
	"saved" => "Handle saved usernames",
	"exit" => "Stop this software",

	"myprofile" => "Go to your profilepage",
	"search" => "Search products",

];

echo "Welcome to huutonet cli.\n";
echo "Help command works everywhere"; // It's a lie

while ( true )
{
	$terminal->page = "Frontpage";
	$terminal->loadBasicScreen();$terminal->loadBasicScreen();

	$suggestions = array_combine(array_keys($commands), array_keys($commands));
	$input = input("> ", $suggestions);

	switch($input) {
	case "help":
		foreach($commands as $command => $description)
			echo "\t '" . tobold($command) . "' - $description\n";
		break;
		
	case "register":
		echo "Not available via api, use https://huuto.net/ to register.\n";
		break;

	case "login":
		$accounts = file_get_contents(BOTFILES . "accounts.json");
		$accounts = json_decode($accounts);
		$accounts = array_combine($accounts, $accounts); // arr(0 => "this") -> arr("this" => "this")
		$username = input("Username: ", $accounts);
		$password = input("Password: ", password: true);
		if( $huuto->login($username, $password) )
		{
			$terminal->username = $username;
			$accountChosen = true;
			$terminal->clearScreen();

			if(
				!in_array($username, $accounts) &&
				readline("Save this username? ")[0] == "y"
			)
			{
				$accounts[] = $username;
				file_put_contents(BOTFILES . "accounts.json", json_encode($accounts));
			}
		}
		break;

	case "logout":
		$huuto->logout();
		$terminal->username = null;
		break;

	case "myprofile":
		$huuto->ownProfile();
		break;

	case "search":
		$searchParameters = $huuto->createSearchParameters();

		$json = $huuto->searchProducts(...$searchParameters);
		$json = json_decode($json);

		foreach($json->items as $item)
			echo "\t$item->id. " . tobold($item->title) . "\n";
		break;

	case "exit":
		exit;
		curl_close($ch);
		break;

	default:
		echo tored("Invalid option.") . "\n";
		break;

	}

}
curl_close($ch);
