<?php
define('HUUTOBOT_USERAGENT', 'Huutonet cli v0.1');
define('HUUTOBOT_BASEURL', 'https://api.huuto.net/1.1/');

$defaultSellOptions = [
        "buyNowPrice"			=> null,
        "categoryId"			=> null,
	"closingTime"			=> null,
	"condition"			=> null, // new, like-new, good, acceptable, weak
	"description"			=> "Tämä tuote on joko bugi tai testi, mutta se ei ole myynnissä.\nTämän tuotteen on laittanut myyntiin oma huutonet selaimeni joka on tällähetkellä kehityksen alla.\n\n[Gitlab - MrDarkHooD - Terminal huutonet helper](https://gitlab.com/MrDarkHooD/terminal-huutonet-helper)",
	"deliveryMethods"		=> [ "pickup", "shipment" ], //array ( pickup, shipment )
	"deliveryTerms" 		=> "",
	"identificationRequired"	=> true, // bool
	"isLocationAbroad"		=> false,
	"listTime"			=> null, //2016-11-20 19:26:00
	"openDays"			=> 14,
	"paymentMethods"		=> [], // array(wire-transfer, cash, mobile-pay)
	"paymentTerms"			=> "",
	"postalCode"			=> 00000,
	"quantity"			=> 1,
	"saleMethod"			=> "buy-now", // auction, buy-now
	"status"			=> "draft", // draft, preview, published, closed, disabled, waiting
	"title"				=> null,
	"offersAllowed" 		=> true, // bool

	"auctionOptions" => [
		"startingPrice" => 0,
	],
	"companyOptions" => [
		"marginalTax"	=> 0,
		"vat"		=> 24,
	],
	"huutoplusOptions" => [
		"minimumFeedback"	=> 0,
		"minimumIncrease"	=> 0.5
	]
];
